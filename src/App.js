import './App.scss';
import React from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';

import Header from "../src/app/ui/header/header"
import Home from "../src/app/pages/home/home"
// import About from "../src/app/pages/about/about"
// import Contact from "../src/app/pages/contact/contact"
import Resume from "../src/app/pages/resume/resume"

const App = () =>{	
	return (
		<div className="App">

			<Router>
				<div class="grid-container">
					<div class="header">
						<Header />
					</div>

					<div class="main-content">
						<Switch>
							<Route exact path="/">
								<Home />
							</Route>
							{/* <Route exact path="/about"> */}
								{/* <About /> */}
							{/* </Route> */}
							<Route exact path="/contact">
								{/* <Contact /> */}
							</Route>
							<Route exact path="/resume">
								<Resume />
							</Route>
						</Switch>
					</div>

					<div class="sidenav">
						<ul>
							<li>
								<NavLink to="/" activeClassName='is-active' class="link"><h1>Accueil</h1></NavLink>
							</li>
							{/* <li>
								<NavLink to="/about" activeClassName='is-active' class="link"><h1>À Propos</h1></NavLink>
							</li> */}
							<li>
								<NavLink to="/contact" activeClassName='is-active' class="link"><h1>Contact</h1></NavLink>
							</li>
							<li>
								<NavLink to="/resume" activeClassName='is-active' class="link"><h1>C.V.</h1></NavLink>
							</li>
						</ul>
					</div>
					<div class="footer">
						<h4>2020 - Découverte de React</h4>
					</div>
				</div>
			</Router>

		</div>
  	);
}

export default App;
