import React from 'react';
import './home.scss';
import Portfolio from "../../features/portfolio/portfolio"


const Home = () =>{

    return(
        <div class="home-wrapper">
            <h1>Bienvenue sur mon site de présentation</h1>
            <div class="description-wrapper">
                <p class="text">Vous retrouverez dans ce petit site de démonstration mes compétences, 
                    ainsi que des choses qui me passionnent.
                <br />
                <br />
                    Ce site est une expérimentation de layout épuré, voué à mettre en avant la force de la défonce
                    blanc sur noir.
                <br />
                <br />
                <br />
                <br />
                </p>
                <p class="text">
                    Actuellement en 5ème année spécialité développement web, il s'agit en réalité de ma 6ème 
                    sur le campus Ynov Toulouse.
                    <br />
                    <br />
                    Avant de partir dans l'informatique, je souhaitais devenir designer de produit afin de concevoir 
                    des meubles. La réalité de l'emploi et le goût pour le design graphique et le code m'ont fait 
                    changer de bord.
                    <br />
                    <br />
                    En changeant pour une filière informatique, notamment pour la sécurité de l'emploi,
                    j'ai pu concillier design et développement. Continuer en parallèle à cultiver ma culture grapghique
                    était pour moi quelque chose de primordial.
                </p>
            </div>

            <h2>Découvrez ci-dessous certaines de mes créations :</h2>
            <div class="project">
                <br />
                <Portfolio></Portfolio>
            </div>

        </div>
    )
}

export default Home;