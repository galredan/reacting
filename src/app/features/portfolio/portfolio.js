import React from 'react';
import './portfolio.scss';
// import images from './../../../assets/images';

let photos =  [
    {
        url: './images/1.png'
    },
    {
        url: './images/2.png'
    },
    {
        url: './images/3.png'
    },
    {
        url: './images/4.png'
    },
    {
        url: './images/5.png'
    }
]

export const Portfolio = () => {
    return (   
        <div class="portfolio">
            {photos.map((image) => {
                return (
                    <div>
                        <img key="{image}" src={image.url} alt="" class="project" />
                    </div>)
                })
            }
        </div>
        
    )
}

export default Portfolio
