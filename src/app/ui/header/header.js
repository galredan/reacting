import './header.scss';
import React from 'react';
import { BrowserRouter as NavLink, Link} from 'react-router-dom';

import Logo from '../../../assets/icons/logo.svg';
import Linkedin from '../../../assets/icons/linkedin.svg';
import Pinterest from '../../../assets/icons/pinterest.svg';
import Facebook from '../../../assets/icons/facebook.svg';
import Instagram from '../../../assets/icons/instagram.svg';

// import Home from "../src/app/pages/home/home"

function Header() {

	return (
        <div class="header-wrap">
            <div class="logo-wrap">
                <Link to={"/"}><img src={Logo} alt="logo lion" id="logo" /></Link>
            </div>
            <h2>Léo Baratgin</h2>


            <div class="social-wrap">
                <a target="_blank" rel="noopener noreferrer" href="https://www.pinterest.fr/galredan/" class="social">
                    <img src={Linkedin} alt="lien linkedin" ></img>
                </a>
                <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/leobaratgin/" class="social">
                    <img src={Pinterest} alt="lien pinterest"></img>
                </a>
                <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/"  class="social">
                    <img src={Facebook} alt="lien facebook"></img>
                </a>
                <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/baratgin_/"  class="social">
                    <img src={Instagram} alt="lien instagram"></img>
                </a>
            </div>
        </div>
    )}

export default Header;
